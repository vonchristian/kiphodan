AccountingModule::Asset.create!(code: 111601, name: "Revolving Fund (Cash Advance)")
AccountingModule::Asset.create!(code: 1113021, name: "Cash in Bank(Common Fund LBP)")
AccountingModule::Asset.create!(code: 1113022, name: "Cash in Bank(MAS Fund LBP)")
AccountingModule::Asset.create!(code: 1113023, name: "Cash in Bank(PNB BIG)")
AccountingModule::Asset.create!(code: 1113024, name: "Cash in Bank(PNB OFW Fund)")
AccountingModule::Asset.create!(code: 1113025, name: "Cash in Bank(RBBI Lamut)")

AccountingModule::Asset.create!(code: 1113026, name: "Other Funds and Deposit (MSCB)")
AccountingModule::Asset.create!(code: 1113027, name: "Other Funds and Deposit (Banco Lagawe - Time Deposit)")
AccountingModule::Asset.create!(code: 1113028, name: "Other Funds and Deposit (Banco Lagawe - Retirement Fund)")
AccountingModule::Asset.create!(code: 1113029, name: "Other Funds and Deposit (Banco Lagawe - Reserve Fund)")
AccountingModule::Asset.create!(code: 11130230, name: "Other Funds and Deposit (Banco Lagawe - CETF Fund)")
AccountingModule::Asset.create!(code: 11130231, name: "Other Funds and Deposit (Banco Lagawe - CDF Fund)")
AccountingModule::Asset.create!(code: 11130232, name: "Other Funds and Deposit (Banco Lagawe - Special Savings)")
AccountingModule::Asset.create!(code: 11130233, name: "Other Funds and Deposit (CLIMBS - Optional Fund)")

AccountingModule::Asset.create!(code: 117001, name: "Loans Receivables - Post Dated Check Loan")
AccountingModule::Asset.create!(code: 117002, name: "Loans Receivables - Soft Loan")
AccountingModule::Asset.create!(code: 117003, name: "Loans Receivables - LBP-Micro Loan")
AccountingModule::Asset.create!(code: 117004, name: "Loans Receivables - DOLE Preseed Loan")
AccountingModule::Asset.create!(code: 117005, name: "Loans Receivables - DTI NGO MCP2 Loan")

AccountingModule::Asset.create!(code: 117601,  name: "Receivable from Gail Dulnuan")
AccountingModule::Asset.create!(code: 117602,  name: "Receivable from Bernard Indopia")
AccountingModule::Asset.create!(code: 117603,  name: "Receivable from Froylan D. Tuguinay")
AccountingModule::Asset.create!(code: 117604,  name: "Receivable from Ronald T. Martin Jr")

AccountingModule::Asset.create!(code: 117215, name: "Accounts Receivables - (Kiangan Storefront)")
AccountingModule::Asset.create!(code: 117216, name: "Total Trade and Other Receivable")

AccountingModule::Asset.create!(code: 118101,  name: "Merchandise Inventory (Kiangan Storefront)")
AccountingModule::Asset.create!(code: 118102,  name: "Merchandise Inventory (Eload)")

AccountingModule::Asset.create!(code: 134004,  name: "Investment - NORLU CF")
AccountingModule::Asset.create!(code: 134005,  name: "Investment - NORLU BF")
AccountingModule::Asset.create!(code: 134006,  name: "Investment - IFEDECO")
AccountingModule::Asset.create!(code: 134007,  name: "Investment - CLIMBS")
AccountingModule::Asset.create!(code: 134008,  name: "Investment - NATTCO")
AccountingModule::Asset.create!(code: 134009,  name: "Investment - MSCB")
AccountingModule::Asset.create!(code: 1340010,  name: "Investment - BRANCH")
AccountingModule::Asset.create!(code: 1340011,  name: "Investment - NELCO LEAGUE")

AccountingModule::Asset.create!(code: 141101,  name: "Land in Quirino")
AccountingModule::Asset.create!(code: 141102,  name: "Land in Nayon")

AccountingModule::Asset.create!(code: 141301,  name: "Building - Bodega")
AccountingModule::Asset.create!(code: 141302,  name: "Building - Kiangan")
AccountingModule::Asset.create!(code: 141311,  name: "Accumulated Depreciation - Buiding Kiangan")
AccountingModule::Asset.create!(code: 141312,  name: "Accumulated Depreciation - Buiding Bodega")

AccountingModule::Asset.create!(code: 179001,  name: "Miscellaneous Assets - Assets Acquired in Settlement")
AccountingModule::Asset.create!(code: 179002,  name: "Water Tank and Cistern")
AccountingModule::Asset.create!(code: 179103,  name: "Accumulated Depreciation - Water Tank and Cistern")

AccountingModule::Asset.create!(code: 179003,  name: "Communication Facility")
AccountingModule::Asset.create!(code: 179103,  name: "Accumulated Depreciation - Communication Facility")

AccountingModule::Asset.create!(code: 173002,  name: "Ecoop Software Program")
AccountingModule::Asset.create!(code: 173010,  name: "Accumulated Depreciation - Ecoop Software Program")

AccountingModule::Liability.create!(code: 213003, name: 'Accounts Payable - Alvarez Farm Supply')
AccountingModule::Liability.create!(code: 213004, name: 'Accounts Payable - Laicom Sales and Promo')
AccountingModule::Liability.create!(code: 213005, name: 'Accounts Payable - Kaneko Seeds')
AccountingModule::Liability.create!(code: 213006, name: 'Accounts Payable - FJB-JAB Enterprise')
AccountingModule::Liability.create!(code: 213007, name: 'Accounts Payable - JMS')
AccountingModule::Liability.create!(code: 213008, name: 'Accounts Payable - Isabela RC Commercial')
AccountingModule::Liability.create!(code: 213009, name: 'Accounts Payable - Rebisco')
AccountingModule::Liability.create!(code: 2130010, name: 'Accounts Payable - G.V. Cosmetics')
AccountingModule::Liability.create!(code: 2130011, name: 'Accounts Payable - D Maya')
AccountingModule::Liability.create!(code: 2130012, name: 'Accounts Payable - San Miguel Foods Inc.')
AccountingModule::Liability.create!(code: 2130013, name: 'Accounts Payable - Wilmels Store')
AccountingModule::Liability.create!(code: 2130014, name: 'Accounts Payable - JL Villarin')
AccountingModule::Liability.create!(code: 2130015, name: 'Accounts Payable - AV Luna')

AccountingModule::Liability.create!(code: 215001, name: 'Loans Payable - P3')
AccountingModule::Liability.create!(code: 215002, name: 'Loans Payable - CECAP')

AccountingModule::Liability.create!(code: 219001, name: 'SSS Premium Payable')
AccountingModule::Liability.create!(code: 219002, name: 'PhilHealth Premium Payable')
AccountingModule::Liability.create!(code: 219003, name: 'PAG-IBIG Premium Payable')
AccountingModule::Liability.create!(code: 219004, name: 'PAG-IBI Loan Payable')
AccountingModule::Liability.create!(code: 219005, name: 'PhilAm Loan Payable')

AccountingModule::Liability.create!(code: 227001, name: 'Due to NORLU CF')
AccountingModule::Liability.create!(code: 227002, name: 'Due to MSCB')
AccountingModule::Liability.create!(code: 227003, name: 'Due to NATTCO')
AccountingModule::Liability.create!(code: 227004, name: 'Due to IFEDECO')
AccountingModule::Liability.create!(code: 227005, name: 'Due to NELCO LEAGUE')


AccountingModule::Liability.create!(code: 231001, name: 'Other Payables - Salaries Treasurer')
AccountingModule::Liability.create!(code: 231002, name: 'Other Payables - Equipment/Ecoop')
AccountingModule::Liability.create!(code: 231003, name: 'Other Payables - Bonus Employees')
AccountingModule::Liability.create!(code: 231004, name: 'Other Payables - Collection Expense')
AccountingModule::Liability.create!(code: 231005, name: 'Other Payables - COLA Treasurer')
AccountingModule::Liability.create!(code: 231006, name: 'Other Payables - PGSIP')
AccountingModule::Liability.create!(code: 231007, name: 'Other Payables - Others')
AccountingModule::Liability.create!(code: 231008, name: 'Other Payables - Honorariums')
AccountingModule::Liability.create!(code: 231009, name: 'Other Payables - Office Building Equipment')
AccountingModule::Liability.create!(code: 2310010, name: 'Other Payables - Warehouse Improvement')
AccountingModule::Liability.create!(code: 2310011, name: 'Other Payables - Generator')
AccountingModule::Liability.create!(code: 2310012, name: 'Other Payables - Freezer')

AccountingModule::Liability.create!(code: 248202, name: "Mutual Assistance System")
AccountingModule::Liability.create!(code: 248203, name: "Retirement Benefit Fund")
AccountingModule::Liability.create!(code: 248204, name: "Emergency Trust Fund")
AccountingModule::Liability.create!(code: 248205, name: "Cooperative Guaranteed Fund")
AccountingModule::Liability.create!(code: 248206, name: "Loan Protection Fund")
AccountingModule::Liability.create!(code: 248207, name: "Contingency Fund")
AccountingModule::Liability.create!(code: 248208, name: "Municipal Cooperative Training Fund")
AccountingModule::Liability.create!(code: 248209, name: "Mutual Assistance System")

AccountingModule::Liability.create!(code: 248301, name: 'Fund from Main Office')

AccountingModule::Equity.create!(code: 305001, name: 'Gawad Pitak Award Fund')
AccountingModule::Equity.create!(code: 305002, name: 'Service Grant from Syngenta')
AccountingModule::Equity.create!(code: 305003, name: 'Donated Fund for Building')
AccountingModule::Equity.create!(code: 305004, name: 'Integrated Rural Finance Award')
AccountingModule::Equity.create!(code: 305005, name: 'Coop NATCCO Fund')

AccountingModule::Equity.create!(code: 306403, name: 'CETF Local 2')
AccountingModule::Equity.create!(code: 306404, name: 'Scholarship Program Fund')


AccountingModule::Asset.create(code: 1171591, contra: true, name: "Unearned Interests - Microfinance Loan")

AccountingModule::Revenue.create(code: 4011091, name: 'Interest Income from Loans - Microfinance Loan')
AccountingModule::Revenue.create(code: 40110921, name: 'Interest Income from Loans - Commercial Loan')
AccountingModule::Revenue.create(code: 40110921, name: 'Interest Income from Loans - Pensioner Loan')
AccountingModule::Revenue.create(code: 40110922, name: 'Interest Income from Loans - OFW Loan')



AccountingModule::Revenue.create(code: 40140191, name: 'Loan Penalties Income - Microfinance Loan')
AccountingModule::Revenue.create(code: 40140192, name: 'Loan Penalties Income - Commercial Loan')
AccountingModule::Revenue.create(code: 40140193, name: 'Loan Penalties Income - Pensioner Loan')
AccountingModule::Revenue.create(code: 40140194, name: 'Loan Penalties Income - Pensioner Loan')
