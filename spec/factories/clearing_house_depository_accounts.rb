FactoryBot.define do
  factory :clearing_house_depository_account do
    depository { nil }
    clearing_house { nil }
    depository_account { nil }
  end
end
