FactoryBot.define do
  factory :membership_category do
    title { "MyString" }
    association :cooperative
  end
end
