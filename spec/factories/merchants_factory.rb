FactoryBot.define do
  factory :merchant do
    name              { "MyString" }
    cooperative       { nil }
    liability_account { nil }
  end
end
